<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
$iconRegistry->registerIcon(
    'frs_linklist-ce-drilldown',
    \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
    array(
        'source' => 'EXT:frs_drk_linklist/Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Drilldown.svg'
    )
);
