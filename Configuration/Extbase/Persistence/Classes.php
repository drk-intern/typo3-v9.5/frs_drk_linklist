<?php

declare(strict_types=1);

return [
    \Frs\FrsDrkLinklist\Domain\Model\Pages::class => [
        'tableName' => 'pages',
    ],
    \Frs\FrsDrkLinklist\Domain\Model\Main::class => [
        'tableName' => 'tx_frsdrklinklist_domain_model_main',
    ],
];
