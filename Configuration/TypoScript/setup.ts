config.tx_extbase {
    persistence {
        classes {
            Frs\FrsDrkLinklist\Domain\Model\Pages {
                mapping {
                    tableName = pages
                }
            }
            Frs\FrsDrkLinklist\Domain\Model\Main {
                mapping {
                    tableName = tx_frsdrklinklist_domain_model_main
                }
            }
        }
    }
}

plugin.tx_frsdrklinklist {
    persistence {
        classes {
            Frs\FrsDrkLinklist\Domain\Model\Main {
                mapping {
                    tableName = tx_frsdrklinklist_domain_model_main
                }
            }
            Frs\FrsDrkLinklist\Domain\Model\Pages {
                mapping {
                    tableName = pages
                }
            }
        }
    }

    view {
        templateRootPath = EXT:frs_drk_linklist/Resources/Private/Templates/

        # Keep option as array
        partialRootPaths {
            0 = EXT:frs_drk_linklist/Resources/Private/Partials/
        }

        #keep option as array
        layoutRootPaths {
            0 = EXT:frs_drk_linklist/Resources/Private/Layouts/
        }
    }
}
