<?php
/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
if (!defined('TYPO3')) {
    die('Access denied.');
}

$langFilePrefix = 'LLL:EXT:frs_drk_linklist/Resources/Private/Language/locallang_be.xlf';

// register this plugin
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'frs_drk_linklist',
    'FrsDrkLinklist',
    'DRK Linklist',
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath('frs_drk_linklist')
    .
    'Resources/Public/Icons/ContentElements/20151130_DRK_Backend_Icons_Drilldown.svg'
);

// add default TypoScript code
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'frs_drk_linklist',
    'Configuration/TypoScript',
    'Linklist'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr(
    'tx_frsdrklinklist_domain_model_main',
    $langFilePrefix
);

// configure tt_content display in BE
$GLOBALS['TCA']['tt_content']['types']['frsdrklinklist_main']['showitem'] = '
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf::palette.general;general,
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.header;header,bodytext,
		tx_frsdrklinklist_linklist_main,
	--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.appearance,
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.frames;frames,
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.textlayout;textlayout,
	--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.visibility;visibility,
		--palette--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:palette.access;access,
	--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.extended';

$GLOBALS['TCA']['tt_content']['types']['frsdrklinklist_main']['columnsOverrides'] = [
    'bodytext' => [
        'config' => [
            'enableRichtext' => true,
        ]
    ]
];

$GLOBALS['TCA']['tt_content']['columns']['CType']['config']['items'][] = [
    $langFilePrefix . ':wizard.title',
    'frsdrklinklist_main',
    ''
];

// configure database field to inline relation
$tempColumnsTtContent = array(
    'tx_frsdrklinklist_linklist_main' => array(
        'exclude' => 0,
        'label' => $langFilePrefix . ':drilldown_item',
        'config' => array(
            'type' => 'inline',
            'foreign_table' => 'tx_frsdrklinklist_domain_model_main',
            'foreign_field' => 'tt_content_uid',
            'foreign_sortby' => 'content_element_sorting',
            'maxitems' => 20,
            'appearance' => array(
                'newRecordLinkAddTitle' => true,
                'collapse' => 0,
                'levelLinksPosition' => 'bottom',
                'showSynchronizationLink' => 0,
                'showPossibleLocalizationRecords' => 0,
                'showAllLocalizationLink' => 0,
                'useSortable' => 1,
                'enabledControls' => array(
                    'info' => false
                ),
            ),
        ),
    ),
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
    'tt_content',
    $tempColumnsTtContent
);
