<?php
if (!defined('TYPO3')) {
    die('Access denied.');
}

$langFilePrefix = 'LLL:EXT:frs_drk_linklist/Resources/Private/Language/locallang_be.xlf';

return array(
    'ctrl' => array(
        'title' => $langFilePrefix . ':title',
        'label' => 'links',
        'label_userFunc' => 'Frs\\FrsDrkLinklist\\Userfuncs\\Tca->getReferencedPageTitle',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => array(
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ),
        'security' => [
            'ignorePageTypeRestriction' => true
        ],
        'searchFields' => 'links',
        'iconfile' => 'EXT:frs_drk_linklist/Resources/Public/Icons/tx_frsdrklinklist_domain_model_main.gif'
    ),
    'types' => array(
        '1' => array(
            'showitem' => 'hidden,--palette--;;1,icon,links,levels,--div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access,starttime,endtime'
        ),
    ),
    'columns' => array(
        'sys_language_uid' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => array(
                'type' => 'select',
                'foreign_table' => 'sys_language',
                'foreign_table_where' => 'ORDER BY sys_language.title',
                'items' => array(
                    array('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages', -1),
                    array('LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.default_value', 0)
                ),
                'renderType' => 'selectSingle',
                'default' => 0,
            ),
        ),
        'l10n_parent' => array(
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array('', 0),
                ),
                'foreign_table' => 'tx_frsdrklinklist_domain_model_main',
                'foreign_table_where' => 'AND tx_frsdrklinklist_domain_model_main.pid=###CURRENT_PID### AND tx_frsdrklinklist_domain_model_main.sys_language_uid IN (-1,0)',
                'renderType' => 'selectSingle',
            ),
        ),
        'l10n_diffsource' => array(
            'config' => array(
                'type' => 'passthrough',
            ),
        ),
        't3ver_label' => array(
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => array(
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            )
        ),
        'hidden' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.hidden',
            'config' => array(
                'type' => 'check',
            ),
        ),
        'starttime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
                'renderType' => 'inputDateTime',
                ['behaviour' => ['allowLanguageSynchronization' => true]],
            ),
        ),
        'endtime' => array(
            'exclude' => 1,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => array(
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'checkbox' => 0,
                'default' => 0,
                'range' => array(
                    'lower' => mktime(0, 0, 0, date('m'), date('d'), date('Y'))
                ),
                'renderType' => 'inputDateTime',
                ['behaviour' => ['allowLanguageSynchronization' => true]],
            ),
        ),
        'icon' => array(
            'exclude' => 1,
            'label' => 'Icon',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array(
                        $langFilePrefix . ':icon.no_icon',
                        'no_icon'
                    ),
                    array(
                        $langFilePrefix . ':icon.landmines',
                        'landmines'
                    ),
                    array(
                        $langFilePrefix . ':icon.als-privatperson-spenden',
                        'als-privatperson-spenden'
                    ),
                    array(
                        $langFilePrefix . ':icon.als-schule-helfen',
                        'als-schule-helfen'
                    ),
                    array(
                        $langFilePrefix . ':icon.als-unternehmen-spenden',
                        'als-unternehmen-spenden'
                    ),
                    array(
                        $langFilePrefix . ':icon.arbeiten-fuer-das-drk',
                        'arbeiten-fuer-das-drk'
                    ),
                    array(
                        $langFilePrefix . ':icon.behindertenhilfe',
                        'behindertenhilfe'
                    ),
                    array(
                        $langFilePrefix . ':icon.bevoelkerungsschutz-und-rettung',
                        'bevoelkerungsschutz-und-rettung'
                    ),
                    array(
                        $langFilePrefix . ':icon.blut-spenden',
                        'blut-spenden'
                    ),
                    array(
                        $langFilePrefix . ':icon.ehrenamt',
                        'ehrenamt'
                    ),
                    array(
                        $langFilePrefix . ':icon.ernaehrungssicherung',
                        'ernaehrungssicherung'
                    ),
                    array(
                        $langFilePrefix . ':icon.erste-hilfe',
                        'erste-hilfe'
                    ),
                    array(
                        $langFilePrefix . ':icon.fluechtlinge',
                        'fluechtlinge'
                    ),
                    array(
                        $langFilePrefix . ':icon.freiwilligendienst',
                        'freiwilligendienst'
                    ),
                    array(
                        $langFilePrefix . ':icon.geldauflagen-bussgeld',
                        'geldauflagen-bussgeld'
                    ),
                    array(
                        $langFilePrefix . ':icon.gesundheit-und-praevention',
                        'gesundheit-und-praevention'
                    ),
                    array(
                        $langFilePrefix . ':icon.gesundheit',
                        'gesundheit'
                    ),
                    array(
                        $langFilePrefix . ':icon.hilfe-fuer-landminen',
                        'hilfe-fuer-landminen'
                    ),
                    array(
                        $langFilePrefix . ':icon.hilfen-in-der-not',
                        'hilfen-in-der-not'
                    ),
                    array(
                        $langFilePrefix . ':icon.katastrophenhilfe',
                        'katastrophenhilfe'
                    ),
                    array(
                        $langFilePrefix . ':icon.katastrophenvorsorge',
                        'katastrophenvorsorge'
                    ),
                    array(
                        $langFilePrefix . ':icon.kinder-jugend-und-familie',
                        'kinder-jugend-und-familie'
                    ),
                    array(
                        $langFilePrefix . ':icon.klimawandel',
                        'klimawandel'
                    ),
                    array(
                        $langFilePrefix . ':icon.kurse-im-ueberblick',
                        'kurse-im-ueberblick'
                    ),
                    array(
                        $langFilePrefix . ':icon.migration',
                        'migration'
                    ),
                    array(
                        $langFilePrefix . ':icon.newsletter-drk',
                        'newsletter-drk'
                    ),
                    array(
                        $langFilePrefix . ':icon.pate-werden',
                        'pate-werden'
                    ),
                    array(
                        $langFilePrefix . ':icon.resilenz',
                        'resilenz'
                    ),
                    array(
                        $langFilePrefix . ':icon.senioren',
                        'senioren'
                    ),
                    array(
                        $langFilePrefix . ':icon.shelter-bauen',
                        'shelter-bauen'
                    ),
                    array(
                        $langFilePrefix . ':icon.sicherung-der-lebensgrundlage',
                        'sicherung-der-lebensgrundlage'
                    ),
                    array(
                        $langFilePrefix . ':icon.spendenservice',
                        'spendenservice'
                    ),
                    array(
                        $langFilePrefix . ':icon.stiftungen',
                        'stiftungen'
                    ),
                    array(
                        $langFilePrefix . ':icon.suchdienst',
                        'suchdienst'
                    ),
                    array(
                        $langFilePrefix . ':icon.wasser-und-hygiene',
                        'wasser-und-hygiene'
                    ),
                    array(
                        $langFilePrefix . ':icon.werben-fuer-das-drk',
                        'werben-fuer-das-drk'
                    ),
                    array(
                        $langFilePrefix . ':icon.wohlfahrtsmarken',
                        'wohlfahrtsmarken'
                    ),
                    array(
                        $langFilePrefix . ':icon.world',
                        'world'
                    )
                ),
                'size' => 1,
                'minitems' => 0,
                'maxitems' => 1,
                'renderType' => 'selectSingle',
            ),
        ),
        'links' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . ':links',
            'config' => array(
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'pages',
                'size' => 1,
                'maxitems' => 1,
                'minitems' => 1
            ),
        ),
        'levels' => array(
            'exclude' => 1,
            'label' => $langFilePrefix . ':levels',
            'config' => array(
                'type' => 'select',
                'items' => array(
                    array(
                        $langFilePrefix . ':levels.1',
                        1
                    ),
                    array(
                        $langFilePrefix . ':levels.2',
                        2
                    ),
                    array(
                        $langFilePrefix . ':levels.3',
                        3
                    ),
                    array(
                        $langFilePrefix . ':levels.4',
                        4
                    ),
                    array(
                        $langFilePrefix . ':levels.5',
                        5
                    ),
                ),
                'size' => 1,
                'maxitems' => 1,
                'renderType' => 'selectSingle',
            ),
        )
    ),
);
