
// configure a drilldown element and place it in menu tab.
mod.wizards.newContentElement.wizardItems.menu {
    elements.frsdrklinklist_main {
        iconIdentifier = frs_linklist-ce-drilldown
        title = LLL:EXT:frs_drk_linklist/Resources/Private/Language/locallang.xlf:wizard.title
        description = LLL:EXT:frs_drk_linklist/Resources/Private/Language/locallang.xlf:wizard.description
        tt_content_defValues {
            CType = frsdrklinklist_main
        }
    }
    show := addToList(frsdrklinklist_main)
}
