<?php

namespace Frs\FrsDrkLinklist\Tests\Unit\Controller;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */
use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use Frs\FrsDrkLinklist\Controller\MainController;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3Fluid\Fluid\View\ViewInterface;

class MainControllerTest extends UnitTestCase
{

    /**
     * @var MainController
     */
    protected $subject = null;

    /**
     * @var ViewInterface
     */
    protected $view = null;

    /**
     * @var MainRepository
     */
    protected $mainRepository = null;

    public function setUp()
    {
        $this->subject = $this->getMock(
            MainController::class,
            array('showAction', 'setView'),
            array(),
            '',
            false
        );

        $this->view = $this->getMock(
            ViewInterface::class
        );
        $this->subject->setView($this->view);

        $this->mainRepository = $this->getMock(
            \Frs\FrsDrkLinklist\Domain\Repository\MainRepository::class,
            array(),
            array(),
            '',
            false
        );
        $this->inject($this->subject, 'mainRepository', $this->mainRepository);
    }

    public function tearDown()
    {
        unset($this->subject, $this->view, $this->mainRepository);
    }

    /**
     * @test
     */
    public function showActionCanBeCalled()
    {
        $this->subject->showAction();
    }

    /**
     * @test
     */
    public function showActionFetchesAllContentsFromRepository()
    {
        $allContents = new ObjectStorage();

        $this->mainRepository->expects($this->any())->method('findAll')
            ->will($this->returnValue($allContents));

        $this->subject->showAction();
    }
}
