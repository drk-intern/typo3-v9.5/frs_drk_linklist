<?php

namespace Frs\FrsDrkLinklist\Tests\Unit\Domain\Model;

use TYPO3\TestingFramework\Core\Unit\UnitTestCase;
use Frs\FrsDrkLinklist\Domain\Model\Main;
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2015 Oliver Wand <systeme@familie-redlich.de>, familie redlich Systeme
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * Test case for class \Frs\FrsDrkLinklist\Domain\Model\Main.
 *
 * @copyright Copyright belongs to the respective authors
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 * @author Oliver Wand <systeme@familie-redlich.de>
 */
class MainTest extends UnitTestCase
{
    /**
     * @var Main
     */
    protected $subject = null;

    protected function setUp()
    {
        $this->subject = new Main();
    }

    protected function tearDown()
    {
        unset($this->subject);
    }


    /**
     * @test
     */
    public function getLinksReturnsInitialValueForInteger()
    {
        $this->assertSame(
            '',
            $this->subject->getLinks()
        );
    }

    /**
     * @test
     */
    public function setLinksForIntegerSetsLinks()
    {
        $this->subject->setLinks(12);

        $this->assertAttributeEquals(
            12,
            'links',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLevelsReturnsInitialValueForInteger()
    {
        $this->assertSame(
            '0',
            $this->subject->getLevels()
        );
    }

    /**
     * @test
     */
    public function setLevelsForIntegerSetsLevels()
    {
        $this->subject->setLevels(1);

        $this->assertAttributeEquals(
            1,
            'levels',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTtContentUidReturnsInitialValueForString()
    {
        $this->assertSame(
            0,
            $this->subject->getTtContentUid()
        );
    }

    /**
     * @test
     */
    public function setTtContentUidForStringSetsTtContentUid()
    {
        $this->subject->setTtContentUid('100');

        $this->assertAttributeEquals(
            '100',
            'ttContentUid',
            $this->subject
        );
    }
}
