<?php

namespace Frs\FrsDrkLinklist\Userfuncs;

use \TYPO3\CMS\Backend\Utility\BackendUtility;

/**
 * Class Tca
 *
 * @package FrsDrkLinklist\Userfuncs
 */
class Tca
{

    /**
     * Gets the title from the referenced page
     *
     * @param array $parameters Parameters used to identify the current record
     * @param object $parentObject Calling object
     *
     * @return void
     */
    public function getReferencedPageTitle(&$parameters, $parentObject)
    {
        $pageId = $parameters['row']['links'];
        // fix for multiple calls of this function. Why does TYPO3 call this function 9 times for only 3 elements.
        $parameters['title'] = '';
        if (is_array($pageId)) {
            if (isset($pageId[0]['title'])) {
                $parameters['title'] = $pageId[0]['title'];
            }
            return;
        }
        if (strpos($pageId, '|') !== false) {
            list($pages, $name) = explode('|', $pageId);
            $parameters['title'] = urldecode($name);
        }
    }
}
