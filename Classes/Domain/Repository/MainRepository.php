<?php

namespace Frs\FrsDrkLinklist\Domain\Repository;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */

use \TYPO3\CMS\Core\Utility\GeneralUtility;
use \TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Annotation as Extbase;

/**
 * The repository for Mains
 */
class MainRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var \Frs\FrsDrkLinklist\Domain\Repository\PagesRepository
     * @Extbase\Inject
     */
    public $pagesRepository;

    /**
     * Find by contentElementUid
     *
     * @param $uid
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByContentElementUid($uid)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->matching(
            $query->equals('tt_content_uid', (int)$uid)
        );
        $query->setOrderings(
            array(
                'content_element_sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
            )
        );

        $result = $query->execute();
        return $this->iteratePages($result);
    }

    /**
     * Returns an array of pages plus child pages
     *
     * @param $pageTrees
     *
     * @return array
     */
    public function iteratePages($pageTrees)
    {
        $tree = array();
        $cnt = 0;

        foreach ($pageTrees as $pageTree) {
            $pid = $pageTree->getLinks();
            $maxLevel = $pageTree->getLevels();
            $icon = $pageTree->getIcon();
            $page = $this->pagesRepository->findByUid($pid);
            if ($page !== null) {
                $tree[$cnt]['data'] = $page;
                $tree[$cnt]['icon'] = $icon;
                $tree[$cnt]['children'] = $this->findRecursive($page->getUid(), $maxLevel, 1);
            }
            $cnt++;
        }
        return $tree;
    }

    /**
     * Function finds current page and calls oneself if maxLevel not reached and child page are found.
     *
     * @param $pid
     * @param $maxLevel
     * @param int $currentLevel
     *
     * @return array
     */
    private function findRecursive($pid, $maxLevel, $currentLevel = 1)
    {
        $pagesArray = array();
        // do we have the maximum level reached
        if ($maxLevel > $currentLevel) {
            // find page by pid
            $pages = $this->pagesRepository->findByPid($pid);
            if ($pages !== null) {
                $currentLevel++;
                // iterate over all pages by pid
                foreach ($pages as $page) {
                    $pagesArray[] = array(
                        // set current page data
                        'data' => $page,
                        // call findRecursive again to get children
                        'children' => $this->findRecursive($page->getUid(), $maxLevel, $currentLevel),
                        // append currentLevel
                        'currentLevel' => $currentLevel
                    );
                }
            }
        }
        return $pagesArray;
    }
}
