<?php

namespace Frs\FrsDrkLinklist\Domain\Repository;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */

use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use \TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * The repository for Mains
 */
class PagesRepository extends Repository
{

    /**
     * Init repository with disrespect storage page
     */
    public function initializeObject()
    {
        $querySettings = $this->getQuerySettings();
        $querySettings->setRespectStoragePage(false);
        $querySettings->setIgnoreEnableFields(false);
        $querySettings->setIncludeDeleted(false);
        $this->setDefaultQuerySettings($querySettings);
    }

    /**
     * @return QuerySettingsInterface
     */
    public function getQuerySettings()
    {
        return $this->objectManager->get(Typo3QuerySettings::class);
    }
}
