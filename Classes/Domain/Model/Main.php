<?php

namespace Frs\FrsDrkLinklist\Domain\Model;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */

/**
 * Main
 */
class Main extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * links
     *
     * @var string
     */
    protected $links = '';

    /**
     * levels
     *
     * @var int
     */
    protected $levels = '0';

    /**
     * Icon
     *
     * @var string
     */
    protected $icon = '';

    /**
     * ttContentUid
     *
     * @var string
     */
    protected $ttContentUid = 0;

    /**
     * Returns the links
     *
     * @return string $links
     */
    public function getLinks()
    {
        return $this->links;
    }

    /**
     * Sets the links
     *
     * @param string $links
     *
     * @return void
     */
    public function setLinks($links)
    {
        $this->links = $links;
    }

    /**
     * Returns the levels
     *
     * @return integer $levels
     */
    public function getLevels()
    {
        return $this->levels;
    }

    /**
     * Sets the levels
     *
     * @param integer $levels
     *
     * @return void
     */
    public function setLevels($levels)
    {
        $this->levels = $levels;
    }

    /**
     * Returns the ttContentUid
     *
     * @return string $ttContentUid
     */
    public function getTtContentUid()
    {
        return $this->ttContentUid;
    }

    /**
     * Sets the ttContentUid
     *
     * @param string $ttContentUid
     *
     * @return void
     */
    public function setTtContentUid($ttContentUid)
    {
        $this->ttContentUid = $ttContentUid;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param int $icon
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }
}
