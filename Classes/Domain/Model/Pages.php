<?php

namespace Frs\FrsDrkLinklist\Domain\Model;

/*
 *    _______________
 *    |       .-.   |
 *    |      // ``  |
 *    |     //      |
 *    |  == ===-_.-'|
 *    |   //  //    |
 *    |__//_________|
 *
 * Copyright (c) 2016 familie-redlich :systeme <systeme@familie-redlich.de>
 *
 * @link     http://www.familie-redlich.de
 * @package  DRK
 *
 */

/**
 * Pages
 */
class Pages extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * uid
     *
     * @var int
     */
    protected $uid;

    /**
     * pid
     *
     * @var int
     */
    protected $pid;

    /**
     * title
     *
     * @var string
     */
    protected $title;

    /**
     * title
     *
     * @var string
     */
    protected $navTitle;

    /**
     * Doktype
     *
     * @var string
     */
    protected $doktype;

    /**
     * @return string
     */
    public function getNavTitle()
    {
        return $this->navTitle;
    }

    /**
     * @param string $navTitle
     */
    public function setNavTitle($navTitle)
    {
        $this->navTitle = $navTitle;
    }

    /**
     * Returns the uid
     *
     * @return int
     */
    public function getUid(): ?int
    {
        return $this->uid;
    }

    /**
     * Returns the pid
     *
     * @return int
     */
    public function getPid(): ?int
    {
        return $this->pid;
    }

    /**
     * Returns the title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDoktype()
    {
        return $this->doktype;
    }

    /**
     * @param string $doktype
     */
    public function setDoktype($doktype)
    {
        $this->doktype = $doktype;
    }
}
